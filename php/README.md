# TMS base PHP images

Docker images built on top of the official PHP images with the addition of some common and useful extensions and tools to develop, test and operate the backend components of TMS.

## Supported tags
* `7.4` [Dockerfile](php/7.4/Dockerfile)
* `8.2` [Dockerfile](php/8.2/Dockerfile)

## PHP extensions
* mbstring
* pdo_mysql
* curl
* json
* intl
* gd
* xml
* zip
* bz2
* opcache
* exif

## PHP extensions
* bz2
* Core
* ctype
* curl
* date
* dom
* exif
* fileinfo
* filter
* ftp
* gd
* hash
* iconv
* intl
* json
* libxml
* mbstring
* mysqlnd
* opcache
* openssl
* pcre
* PDO
* pdo_mysql
* pdo_sqlite
* Phar
* posix
* readline
* Reflection
* session
* SimpleXML
* sodium
* SPL
* sqlite3
* standard
* tokenizer
* xml
* xmlreader
* xmlwriter
* zip
* zlib


## Tools
* Xdebug
* Composer
